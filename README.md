<h1>Space Shooter</h1>

<h4>Simple space shooter clone created with use of PyGame library.</h4>
<h4>Purpose of game is to gain highest score possible.</h4>
<p/>
<h5>Two types of enemies:</h5>
<p>- asteroid (1 point)</p>
<p>- ufo (4 points)</p>
<hr/>
<h4>Control:</h4>
<p></p>
<p>- keybord arrows: ← → ↓ ↑</p>
<p>- launch rocket: [space]</p>
<p>- pause: [esc]</p>

<hr/>

<h2>Project setup</h2>
<h5>Recommended IDE for 'Space Shooter' is PyCharm</h5>
<h5>To make game work:</h5>
<p>1) Open app_start.py with PyCharm</p>
<p>2) Select python interpreter: File->Project Interpreter->Select interpreter localization</p>
<p>3) Setup Pygames library (if not done automatically by PyCharm): File->Project Interpreter->click '+' (install)->type Pygame->Install Package</p>

<hr/>

<h6>Python version: 3.6 </h6>
<h6>Pygame version: 1.9.4 </h6>